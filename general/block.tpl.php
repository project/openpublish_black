<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?> clearfix">

  <?php if (!empty($block->subject) && $block->module != 'views'): ?>
    <h2><span class="black-bg"><?php print "$block->subject"; ?>&nbsp;</span></h2>
  <?php endif;?>

  <div class="content">
    <?php print $block->content ?>
  </div>

</div>
